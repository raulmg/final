import images
def change_colors(image: list[list[tuple[int, int, int]]],
                  to_change: tuple[int, int, int],
                  to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    (width, height) = images.size(image)
    for x in range(width):
        for y in range(height):
            if image[x][y] == to_change:
                image[x][y] = to_change_to
    return image
def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    (width, height) = images.size(image)
    image_rotate = images.create_blank(width, height)
    for x in range(width):
        for y in range(height):
            image_rotate [x][y] = image [y][x]
    return image_rotate
def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    (width, height) = images.size(image)
    image_mirror = images.create_blank(width, height)

    for x in range(width):
        for y in range(height):
            image_mirror[x][y] = image [width-x][y]
    return image_mirror
def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    (width, height) = images.size(image)
    for x in range(width):
        for y in range(height):
            red = image[x][y][0] + increment
            green = image[x][y][1] + increment
            blue = image[x][y][2] + increment
            if red > 255:
                red = 0
            if red < 0:
                red = 255
            if green > 255:
                green = 0
            if green < 0:
                green = 255
            if blue > 255:
                blue = 0
            if blue < 0:
                blue = 255
            pixel = (red, green, blue)
            image[x][y] = pixel
    return image
def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    (width, height) = images.size(image)
    for x in range(width):
        for y in range(height):
            arriba = image[x][y+1]
            abajo = image[x][y-1]
            derecha = image[x+1][y]
            izquierda = image[x-1][y]
            red = (arriba[0] + abajo[0] + derecha[0] + izquierda[0])/4
            green = (arriba[1] + abajo[1] + derecha[1] + izquierda[1]) / 4
            blue = (arriba[2] + abajo[2] + derecha[2] + izquierda[2]) / 4
            pixel = (red, green, blue)
            image[x][y] = pixel
    return image
def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) ->(
list)[list[tuple[int, int, int]]]:
    copia = image
    (width, height) = images.size(image)
    for x in range(width):
        for y in range(height):
            image[x + horizontal][y + vertical] = copia[x][y]
    return image
def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) \
        ->list[list[tuple[int, int, int]]]:
    cortado = images.create_blank(width,height)
    for i in range(width):
        for j in range(height):
            cortado[i][j] = image[x+i][y+j]
    return cortado
def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    (width, height) = images.size(image)
    for x in range(width):
        for y in range(height):
            red = image[x][y][0]
            green = image[x][y][1]
            blue = image[x][y][2]
            gris = (red + green + blue)/3
            pixel = (gris, gris, gris)
            image[x][y] = pixel
    return image
def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) \
        ->list[list[tuple[int, int, int]]]:
    (width, height) = images.size(image)
    for x in range(width):
        for y in range(height):
            red = image[x][y][0] * r
            green = image[x][y][1] * g
            blue = image[x][y][2] * b
            if red > 255:
                red = 255
            if green > 255:
                green = 255
            if blue > 255:
                blue = 255
            pixel = (red, green, blue)
            image[x][y] = pixel
    return image
